/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}
function myFunction2() {
  document.getElementById("myDropdown2").classList.toggle("show");
}
// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}

jQuery(document).ready(function() {
  jQuery(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');   
});





$(document).ready(function() {
  $('a[href*=#]').bind('click', function(e) {
      e.preventDefault(); // prevent hard jump, the default behavior

      var target = $(this).attr("href"); // Set the target as variable

      // perform animated scrolling by getting top-position of target-element and set it as scroll target
      $('html, body').stop().animate({
          scrollTop: $(target).offset().top
      }, $(this).height(), function() {
          location.hash = target; //attach the hash (#jumptarget) to the pageurl
      });

      return false;
  });
});

$(window).scroll(function() {
  var scrollDistance = $(window).scrollTop();

  // Show/hide menu on scroll
  //if (scrollDistance >= 850) {
  //		$('nav').fadeIn("fast");
  //} else {
  //		$('nav').fadeOut("fast");
  //}

  // Assign active class to nav links while scolling
  $('.page-section').each(function(i) {
      if ($(this).position().top <= scrollDistance) {
          $('.navigation a.active').removeClass('active');
          $('.navigation a').eq(i).addClass('active');
      }
      
      console.log("$(this).position().top" +$(this).position().top);
      console.log("$(this).height()" +$(this).height());
      console.log("scrollDistance" +scrollDistance);
  });
}).scroll();


$(document).on('click', '.modal a', function() {
  $(this).closest(".modal").modal("hide");
});

$(document).ready(function(){

  $("#show_password").on('click',function(){
    var PasswordFeild =$("#Password");
    var PasswordFeildType =PasswordFeild.attr('type');
    console.log(PasswordFeildType);
    if(PasswordFeildType=="password")
    {
      PasswordFeild.attr('type','text');
      $(this).text('hide');
    }
    else
    {
      PasswordFeild.attr('type','password');
      $(this).text('show');
    }

  });
});

 
// var elementPosition = $('#PageDetailContainer').offset();

// $(window).scroll(function(){
//         if($(window).scrollTop() > elementPosition.top){
//               $('#mainNav').addClass('navigation_after').css('position','fixed').css('bottom','0');
//               $('#mainNav');
//         } else {
//             $('#mainNav').css('position','relative');
//             $('#mainNav').removeClass('navigation_after')
//         }    
// });

var elementPosition = $('#PageDetailContainer').offset();

$(window).scroll(function(){
        if($(window).scrollTop() > elementPosition.top){
              $('#mainNav').addClass('navigation_after').css('position','fixed').css('bottom','0');
			//   if($(window).onscroll()){
			// 	  for (let index = -0; index > -600; index-50) {
			// 		  $('#mainNav').css('top',index)
			// 	  }
			//  } 
        } else {
            $('#mainNav').css('position','relative');
            $('#mainNav').removeClass('navigation_after')
        }    
});




var elementDisplacement = $('#8').offset();

$(window).scroll(function(){
  var scrolled = elementDisplacement.top - $(window).scrollTop();
        if($(window).scrollTop() > elementDisplacement.top){
			$('#mainNav').css('top','-200px');
        } else {
			$('#mainNav').css('top','0px');
        }    
        if(($(window).scrollTop() - elementDisplacement.top) > 200){
          $('#mainNav').css('top','-400px');
         }
         if(($(window).scrollTop() - elementDisplacement.top) > 400){
          $('#mainNav').css('top','-400px');
         }
         if(($(window).scrollTop() - elementDisplacement.top) > 600){
          $('#mainNav').css('top','-600px');
         }
});


// var FooterPosition = $('#FooterBottom').offset();
// var scrollBottom = window.scrollTo(0,document.body.scrollHeight);
// $(window).scroll(function(){
//   if($(window).scrollBottom<=FooterPosition){
//     $('#mainNav').css('position','relative');
//   }  
// });

jQuery(document).ready(function () {
	jQuery(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');
	var instance = new Mark(document.querySelector("#bodyContainer"));
	$("#search").keyup(function () {
	  var searchTerm = $(this).val();
	  instance.unmark().mark(searchTerm);
	}).trigger("input.highlight").focus();
  });

  var x, i, j, selElmnt, a, b, c;
/* Look for any elements with the class "custom-select": */
x = document.getElementsByClassName("custom-dropdown");
for (i = 0; i < x.length; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    /* For each element, create a new DIV that will act as the selected item: */
    a = document.createElement("DIV");
    a.setAttribute("class", "select-selected");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);
    /* For each element, create a new DIV that will contain the option list: */
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 1; j < selElmnt.length; j++) {
        /* For each option in the original select element,
        create a new DIV that will act as an option item: */
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function (e) {
            /* When an item is clicked, update the original select box,
            and the selected item: */
            var y, i, k, s, h;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            h = this.parentNode.previousSibling;
            for (i = 0; i < s.length; i++) {
                if (s.options[i].innerHTML == this.innerHTML) {
                    s.selectedIndex = i;
                    h.innerHTML = this.innerHTML;
                    y = this.parentNode.getElementsByClassName("same-as-selected");
                    for (k = 0; k < y.length; k++) {
                        y[k].removeAttribute("class");
                    }
                    this.setAttribute("class", "same-as-selected");
                    break;
                }
            }
            h.click();
        });
        b.appendChild(c);
    }
    x[i].appendChild(b);
    a.addEventListener("click", function (e) {
        /* When the select box is clicked, close any other select boxes,
        and open/close the current select box: */
        e.stopPropagation();
        closeAllSelect(this);
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
    });
}

function closeAllSelect(elmnt) {
    /* A function that will close all select boxes in the document,
    except the current select box: */
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);